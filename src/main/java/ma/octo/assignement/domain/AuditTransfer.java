package ma.octo.assignement.domain;

import lombok.*;
import ma.octo.assignement.domain.util.EventType;

import javax.persistence.*;

@Entity
@Table(name = "AUDIT_TRANSFER")
@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AuditTransfer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;



}

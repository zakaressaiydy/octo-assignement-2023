package ma.octo.assignement.mapper;



import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;

public class UtilisateurMapper {


    public UtilisateurDto toDto(Utilisateur utilisateur) {
        if ( utilisateur == null ) {
            return null;
        }

        UtilisateurDto.UtilisateurDtoBuilder utilisateurDto = UtilisateurDto.builder();

        utilisateurDto.id( utilisateur.getId() );
        utilisateurDto.username( utilisateur.getUsername() );
        utilisateurDto.gender( utilisateur.getGender() );
        utilisateurDto.lastname( utilisateur.getLastname() );
        utilisateurDto.firstname( utilisateur.getFirstname() );
        utilisateurDto.birthdate( utilisateur.getBirthdate() );

        return utilisateurDto.build();
    }


    public Utilisateur toModel(UtilisateurDto utilisateurDto) {
        if ( utilisateurDto == null ) {
            return null;
        }

        Utilisateur utilisateur = new Utilisateur();

        utilisateur.setId( utilisateurDto.getId() );
        utilisateur.setUsername( utilisateurDto.getUsername() );
        utilisateur.setGender( utilisateurDto.getGender() );
        utilisateur.setLastname( utilisateurDto.getLastname() );
        utilisateur.setFirstname( utilisateurDto.getFirstname() );
        utilisateur.setBirthdate( utilisateurDto.getBirthdate() );

        return utilisateur;
    }


}


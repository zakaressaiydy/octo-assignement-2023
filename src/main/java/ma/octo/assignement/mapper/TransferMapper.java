package ma.octo.assignement.mapper;


import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;

public class TransferMapper {


    public TransferDto toDto(Transfer transfer) {
        if ( transfer == null ) {
            return null;
        }

        TransferDto transferDto = new TransferDto();
        CompteMapper compteMapper = new CompteMapper();
        transferDto.setId( transfer.getId() );
        transferDto.setCompteEmetteur( compteMapper.toDto( transfer.getCompteEmetteur() ) );
        transferDto.setCompteBeneficiaire( compteMapper.toDto( transfer.getCompteBeneficiaire() ) );
        transferDto.setMotifTransfer( transfer.getMotifTransfer() );
        transferDto.setMontantTransfer( transfer.getMontantTransfer() );
        transferDto.setDateExecution( transfer.getDateExecution() );

        return transferDto;
    }

    public Transfer toModel(TransferDto transferDto) {
        if ( transferDto == null ) {
            return null;
        }

        Transfer transfer = new Transfer();
        CompteMapper compteMapper = new CompteMapper();
        transfer.setId( transferDto.getId() );
        transfer.setMontantTransfer( transferDto.getMontantTransfer() );
        transfer.setDateExecution( transferDto.getDateExecution() );
        transfer.setCompteEmetteur( compteMapper.toModel( transferDto.getCompteEmetteur() ) );
        transfer.setCompteBeneficiaire( compteMapper.toModel( transferDto.getCompteBeneficiaire() ) );
        transfer.setMotifTransfer( transferDto.getMotifTransfer() );

        return transfer;
    }

}

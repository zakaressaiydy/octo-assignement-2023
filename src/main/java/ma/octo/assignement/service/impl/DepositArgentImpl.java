package ma.octo.assignement.service.impl;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.DepositArgent;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.DepositArgentDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantNonAutoriserException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositArgentMapper;
import ma.octo.assignement.repository.DepositArgentRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IDepositArgentService;
import ma.octo.assignement.service.common.Utils;
import ma.octo.assignement.service.common.ApplicationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@Slf4j
public class DepositArgentImpl implements IDepositArgentService {


    @Autowired
    DepositArgentRepository depositArgentRepository;

    @Autowired
    private ICompteService compteService;


    @Autowired
    private IAuditService auditService;



    @Override
    public List<DepositArgentDto> findAll() {
        List<DepositArgent> depositArgentList = depositArgentRepository.findAll();

        if (!CollectionUtils.isEmpty(depositArgentList)){
            List<DepositArgentDto> depositArgentDtos = new ArrayList<>();
            DepositArgentMapper depositArgentMapper = new DepositArgentMapper();
            depositArgentList.forEach(depositArgent -> {

                depositArgentDtos.add( depositArgentMapper.toDto(depositArgent));


            });

            return depositArgentDtos;
        }

        return null;
    }


    @Override
    public DepositArgentDto save(DepositArgentDto depositAgentDto) {

        if (depositAgentDto!=null){
            DepositArgentMapper depositArgentMapper = new DepositArgentMapper();
            DepositArgent depositArgent = depositArgentMapper.toModel(depositAgentDto);
            DepositArgent savedDepositArgent =  depositArgentRepository.save(depositArgent);
            return  depositArgentMapper.toDto(savedDepositArgent);

        }
        return null;
    }

    @Override
    public DepositArgentDto depositArgent(DepositArgentDto depositArgentDto) throws CompteNonExistantException, TransactionException, MontantNonAutoriserException {
        CompteDto compteDto = compteService.findByRib(depositArgentDto.getCompteBeneficiaire().getRib());

        if (compteDto == null) {
            log.debug("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }



        if (Utils.isMontantEmpty(depositArgentDto.getMontant())) {

            log.debug("Montant invalid");
            throw new TransactionException("Montant invalid");

        } else if (depositArgentDto.getMontant().intValue() > ApplicationInfo.MONTANT_MAXIMAL) {
            log.debug("Le montant maximal est 10000");
            throw new MontantNonAutoriserException("Le montant maximal est 10000");
        }

        if (Utils.EMPTY_STRING.equals(depositArgentDto.getMotifDeposit())) {
            log.debug("Votre MotifDeposit est vide");
            throw new TransactionException("Votre MotifDeposit est vide");
        }

        if(Utils.EMPTY_STRING.equals(depositArgentDto.getNomPrenomEmetteur())) {
            log.debug("nom vide");
            throw new TransactionException("nom vide");
        }


        compteDto.setSolde(new BigDecimal(compteDto.getSolde().intValue() + depositArgentDto.getMontant().intValue()));
        compteService.save(compteDto);

        DepositArgentDto depositArgentDto1 = new DepositArgentDto();
        depositArgentDto1.setDateExecution(depositArgentDto.getDateExecution());
        depositArgentDto1.setCompteBeneficiaire(compteDto);
        depositArgentDto1.setNomPrenomEmetteur(depositArgentDto.getNomPrenomEmetteur());
        depositArgentDto1.setMotifDeposit(depositArgentDto.getMotifDeposit());
        depositArgentDto1.setMontant(depositArgentDto.getMontant());
        DepositArgentDto save = this.save(depositArgentDto1);

        auditService.auditDeposit("Deposit de M/Mme. " +depositArgentDto.getNomPrenomEmetteur()
                + " , votre RIB est : " + depositArgentDto.getCompteBeneficiaire().getRib()
                + " avec un montant de  " + depositArgentDto.getMontant().toString());


        return save;
    }


}

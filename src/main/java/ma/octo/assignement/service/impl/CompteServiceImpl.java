package ma.octo.assignement.service.impl;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
@Service
public class CompteServiceImpl implements ICompteService {


    @Autowired
    public CompteRepository compteRepository;




    @Override
    public List<CompteDto> findAll() {
        List<Compte> compteList = compteRepository.findAll();

        if (!CollectionUtils.isEmpty(compteList)){
            List<CompteDto> compDtos = new ArrayList<>();
            CompteMapper compteMapper = new CompteMapper();
            compteList.forEach(compte -> {

                compDtos.add(compteMapper.toDto(compte));

            });

            return compDtos;
        }

        return null;
    }

    @Override
    public CompteDto findByNumeroCompte(String numeroCompte) {
         Compte compte = compteRepository.findByNumeroCompte(numeroCompte);
        CompteMapper compteMapper=new CompteMapper();
        if (compte!=null){
            return compteMapper.toDto(compte);
        }

        return null;
    }

    @Override
    public CompteDto save(CompteDto compteDto) {

        if (compteDto!=null){
            CompteMapper compteMapper = new CompteMapper();
            Compte compte = compteMapper.toModel(compteDto);
            Compte savedCompte =  compteRepository.save(compte);
            return  compteMapper.toDto(savedCompte);

        }
        return null;


    }

    @Override
    public CompteDto findByRib(String rib) {

        Compte compte = compteRepository.findByRib(rib);
        if (compte!=null){
            CompteMapper compteMapper = new CompteMapper();
            return compteMapper.toDto(compte);
        }
        return null;
    }
}

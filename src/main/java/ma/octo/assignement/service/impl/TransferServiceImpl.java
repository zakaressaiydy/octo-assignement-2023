package ma.octo.assignement.service.impl;

import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.TransferDto;


import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.ITransferService;
import ma.octo.assignement.service.common.Utils;
import ma.octo.assignement.service.common.ApplicationInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
@Slf4j
public class TransferServiceImpl implements ITransferService {


    @Autowired
    TransferRepository transferRepository;

    @Autowired
    ICompteService compteService;


    @Autowired
    IAuditService auditService;



    @Override
    public TransferDto save(TransferDto transferDto) {

        if (transferDto!=null){
            TransferMapper transferMapper =  new TransferMapper();
            Transfer transfer = transferMapper.toModel(transferDto);
            Transfer savedTransfer =  transferRepository.save(transfer);
            return transferMapper.toDto(savedTransfer);

        }
        return null;

    }

    @Override
    public List<TransferDto> findAll() {
        List<Transfer> transferList = transferRepository.findAll();

        if (!CollectionUtils.isEmpty(transferList)){
            List<TransferDto> transferDtos = new ArrayList<>();
            TransferMapper transferMapper = new TransferMapper();
            transferList.forEach(transfer -> {

                transferDtos.add( transferMapper.toDto(transfer));


            });

            return transferDtos;
        }

        return null;
    }

    @Override
    public TransferDto createTransaction(TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        CompteDto compteEmetteurDto = compteService.findByNumeroCompte(transferDto.getCompteEmetteur().getNumeroCompte());
        CompteDto compteBeneficiaireDto = compteService.findByNumeroCompte(transferDto.getCompteBeneficiaire().getNumeroCompte());

        if (Utils.isCompteDtoNull( compteEmetteurDto )) {

            log.debug("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");

        }

        if (Utils.isCompteDtoNull( compteBeneficiaireDto )) {

            log.debug("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");

        }

        if (Utils.isMontantEmpty( transferDto.getMontantTransfer() )) {

            log.debug("Montant vide");
            throw new TransactionException("Montant vide");

        } else if ( Utils.isMontantDtoLessThenTeen( transferDto.getMontantTransfer() )) {

            log.debug("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");

        } else if (transferDto.getMontantTransfer().intValue() > ApplicationInfo.MONTANT_MAXIMAL) {

            log.debug("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (Utils.EMPTY_STRING.equals(transferDto.getMotifTransfer())) {

            log.debug("Motif vide");
            throw new TransactionException("Motif vide");

        }

        if (compteEmetteurDto.getSolde().intValue() - transferDto.getMontantTransfer().intValue() < 0) {

            log.debug("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");

        }


        UtilisateurMapper utilisateurMapper = new UtilisateurMapper();

        compteEmetteurDto.setSolde(compteEmetteurDto.getSolde().subtract(transferDto.getMontantTransfer()));
        Compte compteEmetteur = new Compte();
        compteEmetteur.setRib(compteEmetteurDto.getRib());
        compteEmetteur.setSolde(compteEmetteurDto.getSolde());
        compteEmetteur.setNumeroCompte(compteEmetteurDto.getNumeroCompte());
        compteEmetteur.setUtilisateur(utilisateurMapper.toModel(compteEmetteurDto.getUtilisateur()));




        compteBeneficiaireDto.setSolde(new BigDecimal(compteBeneficiaireDto.getSolde().intValue() + transferDto.getMontantTransfer().intValue()));
        Compte compteBeneficiaire = new Compte();
        compteBeneficiaire.setRib(compteBeneficiaireDto.getRib());
        compteBeneficiaire.setSolde(compteBeneficiaireDto.getSolde());
        compteBeneficiaire.setNumeroCompte(compteBeneficiaireDto.getNumeroCompte());
        compteBeneficiaire.setUtilisateur(utilisateurMapper.toModel(compteBeneficiaireDto.getUtilisateur()));



        TransferDto transferDto1 = new TransferDto();
        transferDto1.setDateExecution(transferDto.getDateExecution());
        transferDto1.setCompteEmetteur(compteEmetteurDto);
        transferDto1.setCompteBeneficiaire(compteBeneficiaireDto);
        transferDto1.setMotifTransfer(transferDto.getMotifTransfer());
        transferDto1.setMontantTransfer(transferDto.getMontantTransfer());



        compteService.save(compteBeneficiaireDto);
        compteService.save(compteEmetteurDto);
        TransferDto saved = this.save(transferDto);

        auditService.auditTransfer("Transfer depuis " + transferDto.getCompteEmetteur().getNumeroCompte() + " vers " + transferDto
                .getCompteBeneficiaire().getNumeroCompte() + " d'un montant de " + transferDto.getMontantTransfer()
                .toString());

        return  saved;


    }
}

package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.UtilisateurDto;;
import ma.octo.assignement.mapper.UtilisateurMapper;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;


import java.util.ArrayList;
import java.util.List;


@Service
public class UtilisateurServiceImpl implements IUtilisateurService {

    @Autowired
    UtilisateurRepository utilisateurRepository;





    @Override
    public List<UtilisateurDto> findAll() {
        List<Utilisateur> utilisateurList = utilisateurRepository.findAll();

        if (!CollectionUtils.isEmpty(utilisateurList)){
            List<UtilisateurDto> utilisateurDtos = new ArrayList<>();
            utilisateurList.forEach(utilisateur -> {
                UtilisateurMapper utilisateurMapper = new UtilisateurMapper();
                utilisateurDtos.add( utilisateurMapper.toDto(utilisateur));


            });

            return utilisateurDtos;
        }

        return null;
    }


    @Override
    public UtilisateurDto save(UtilisateurDto utilisateurDto) {

            if (utilisateurDto!=null){
                UtilisateurMapper utilisateurMapper = new UtilisateurMapper();
                Utilisateur utilisateur = utilisateurMapper.toModel(utilisateurDto);
                Utilisateur savedUtilisateur =  utilisateurRepository.save(utilisateur);
                return  utilisateurMapper.toDto(savedUtilisateur);

            }
            return null;


    }


    @Override
    public UtilisateurDto findById(Long id) {
        Utilisateur utilisateur = utilisateurRepository.findById(id).orElse(null);

        if (utilisateur!=null){
            UtilisateurMapper utilisateurMapper = new UtilisateurMapper();
            return utilisateurMapper.toDto(utilisateur);
        }

        return null;
    }
}

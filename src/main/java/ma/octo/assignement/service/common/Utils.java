package ma.octo.assignement.service.common;

import ma.octo.assignement.dto.CompteDto;

import java.math.BigDecimal;

public class Utils {


    public static final String EMPTY_STRING = "";


    public static boolean isMontantEmpty(BigDecimal montant){

        return (montant.equals(null) || montant.intValue()==0)?true:false;
    }

    public static boolean isCompteDtoNull(CompteDto compteDto){

        return compteDto == null;
    }


    public static boolean isMontantDtoLessThenTeen(BigDecimal montant){

        return montant.intValue()<10;

    }



}

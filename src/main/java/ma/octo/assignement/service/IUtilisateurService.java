package ma.octo.assignement.service;


import ma.octo.assignement.dto.UtilisateurDto;

import java.util.List;

public interface IUtilisateurService {


    List<UtilisateurDto> findAll();

    UtilisateurDto save(UtilisateurDto utilisateurDto);

    UtilisateurDto findById(Long id);




}

package ma.octo.assignement.service;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;


import java.util.List;

public interface ITransferService {

  TransferDto save(TransferDto transferDto);

  List<TransferDto> findAll();

  TransferDto createTransaction( TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;


}

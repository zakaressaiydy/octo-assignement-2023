package ma.octo.assignement.service;


import ma.octo.assignement.dto.DepositArgentDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantNonAutoriserException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IDepositArgentService {
        List<DepositArgentDto> findAll();

        DepositArgentDto save(DepositArgentDto depositAgentDto);

        DepositArgentDto depositArgent( DepositArgentDto depositArgentDto)
                throws CompteNonExistantException, TransactionException, MontantNonAutoriserException;

}

package ma.octo.assignement.exceptions;

public class SoldeDisponibleInsuffisantException extends Exception {



  public SoldeDisponibleInsuffisantException() {
  }

  public SoldeDisponibleInsuffisantException(String message) {
    super(message);
  }

  public SoldeDisponibleInsuffisantException(String message, Throwable cause) {
    super(message, cause);
  }

  public SoldeDisponibleInsuffisantException(Throwable cause) {
    super(cause);
  }



}

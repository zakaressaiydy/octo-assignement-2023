package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.service.ICompteService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;



@RestController
@Transactional
@RequestMapping(path = "/api/v1/comptes")
@Slf4j
@AllArgsConstructor
public class CompteController {


    private ICompteService compteService;


    @GetMapping
    List<CompteDto> loadAllCompte() {
        return compteService.findAll();
    }

}

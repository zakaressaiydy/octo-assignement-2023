package ma.octo.assignement.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@Transactional
@RequestMapping(path = "/api/v1/utilisateurs")
@Slf4j
public class UtilisateurController {

    @Autowired
    private IUtilisateurService utilisateurService;

    @GetMapping
    List<UtilisateurDto> loadAllUtilisateur() {
        return utilisateurService.findAll();

    }
}

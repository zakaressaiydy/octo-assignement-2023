package ma.octo.assignement.web;


import lombok.extern.slf4j.Slf4j;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;


import ma.octo.assignement.service.ITransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Transactional
@RequestMapping(path = "/api/v1/transfer")
@Slf4j
class TransferController {

    @Autowired
    private ITransferService transferService;



    @GetMapping
    List<TransferDto> loadAll() {
        log.debug("Lister les Transfer");
        return transferService.findAll();

    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

            log.debug("create Transaction ...");
            transferService.createTransaction(transferDto);
        
    }





}

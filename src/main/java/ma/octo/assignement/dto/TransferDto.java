package ma.octo.assignement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class TransferDto {

  private Long id;

  private CompteDto compteEmetteur;

  private CompteDto compteBeneficiaire;

  private String motifTransfer;

  private BigDecimal montantTransfer;

  private Date dateExecution;

}

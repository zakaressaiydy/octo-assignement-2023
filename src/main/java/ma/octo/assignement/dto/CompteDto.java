package ma.octo.assignement.dto;

import lombok.*;
import ma.octo.assignement.domain.Utilisateur;
import org.springframework.stereotype.Component;


import java.math.BigDecimal;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class CompteDto {

    private Long id;

    private String numeroCompte;

    private String rib;

    private BigDecimal solde;

    private UtilisateurDto utilisateur;

}

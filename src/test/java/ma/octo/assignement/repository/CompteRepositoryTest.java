package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;



@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class CompteRepositoryTest {



    @Autowired
    private CompteRepository compteRepository;

    @AfterEach
    void treatDown(){
        compteRepository.deleteAll();
    }

    @Test
    public void checkExitingCompteById() throws ParseException {
        //given


        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);


        Compte compte1 = new Compte();
        compte1.setNumeroCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);





        //when
        Compte savedCompte = compteRepository.save(compte1);
        Compte FindOne = compteRepository.findById(savedCompte.getId()).orElse(null);

        //then
        assertEquals(FindOne.getId(), savedCompte.getId());

    }


    @Test
    public void checkNonExitingTransferById() throws ParseException {
        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);


        Compte compte1 = new Compte();
        compte1.setNumeroCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);


        //when

        Compte FindOne = compteRepository.findById(compte1.getId()+1).orElse(null);

        //then
        assertNull(FindOne);

    }

    @Test
    public void checkAllComptes() {
        ArrayList<Compte> listeDesTransferts =  (ArrayList<Compte>) compteRepository.findAll();
        assertEquals(listeDesTransferts.size(),2);
    }

    @Test
    public void save() throws ParseException {



        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);



        Compte compte1 = new Compte();
        compte1.setNumeroCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        Compte saved = compteRepository.save(compte1);


        assertSame(saved, compte1);
    }

    @Test
    public void delete() throws ParseException {


        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);

        Compte compte1 = new Compte();
        compte1.setNumeroCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        Compte saved = compteRepository.save(compte1);


        compteRepository.delete(saved);

        Compte found = compteRepository.findById(saved.getId()).orElse(null);

        assertNull(found);
    }

}
package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import org.aspectj.weaver.loadtime.Agent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class TransferRepositoryTest {

  @Autowired
  private TransferRepository transferRepository;

  @Autowired
  private CompteRepository compteRepository;


  @Test
  public void checkExitingTransferById() throws ParseException {

    //given
    Transfer transfer = new Transfer();
    Utilisateur utilisateur1 = new Utilisateur();
    utilisateur1.setUsername("user1");
    utilisateur1.setLastname("Ettafssaoui");
    utilisateur1.setFirstname("Youssef");
    utilisateur1.setGender(Gender.MALE);
    Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
    utilisateur1.setBirthdate(dateNaissanceUtilisateur1);

    Utilisateur utilisateur2 = new Utilisateur();
    utilisateur2.setUsername("user2");
    utilisateur2.setLastname("Ettafssaoui");
    utilisateur2.setFirstname("Ahmed");
    utilisateur2.setGender(Gender.MALE);
    Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
    utilisateur2.setBirthdate(dateNaissanceUtilisateur2);

    Compte compte1 = new Compte();
    compte1.setNumeroCompte("010000A000001000");
    compte1.setRib("RIB1");
    compte1.setSolde(BigDecimal.valueOf(200000L));
    compte1.setUtilisateur(utilisateur1);
    compteRepository.save(compte1);

    Compte compte2 = new Compte();
    compte2.setNumeroCompte("010000B025001000");
    compte2.setRib("RIB2");
    compte2.setSolde(BigDecimal.valueOf(140000L));
    compte2.setUtilisateur(utilisateur2);
    compteRepository.save(compte2);

    transfer.setId((long) 1);
    transfer.setMontantTransfer(BigDecimal.TEN);
    transfer.setCompteBeneficiaire(compte2);
    transfer.setCompteEmetteur(compte1);
    transfer.setDateExecution(new Date());
    transfer.setMotifTransfer("Assignment 2021");
    Transfer transfer1 = transferRepository.save(transfer);

    //when
    Transfer FindOne = transferRepository.findById(transfer1.getId()).orElse(null);

    //then
    assertEquals(FindOne.getId(), transfer.getId());

  }

  @Test
  public void checkNonExitingTransferById() throws ParseException {

    //given
    Transfer transfer = new Transfer();
    Utilisateur utilisateur1 = new Utilisateur();
    utilisateur1.setUsername("user1");
    utilisateur1.setLastname("Ettafssaoui");
    utilisateur1.setFirstname("Youssef");
    utilisateur1.setGender(Gender.MALE);
    Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
    utilisateur1.setBirthdate(dateNaissanceUtilisateur1);

    Utilisateur utilisateur2 = new Utilisateur();
    utilisateur2.setUsername("user2");
    utilisateur2.setLastname("Ettafssaoui");
    utilisateur2.setFirstname("Ahmed");
    utilisateur2.setGender(Gender.MALE);
    Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
    utilisateur2.setBirthdate(dateNaissanceUtilisateur2);

    Compte compte1 = new Compte();
    compte1.setNumeroCompte("010000A000001000");
    compte1.setRib("RIB1");
    compte1.setSolde(BigDecimal.valueOf(200000L));
    compte1.setUtilisateur(utilisateur1);
    compteRepository.save(compte1);

    Compte compte2 = new Compte();
    compte2.setNumeroCompte("010000B025001000");
    compte2.setRib("RIB2");
    compte2.setSolde(BigDecimal.valueOf(140000L));
    compte2.setUtilisateur(utilisateur2);
    compteRepository.save(compte2);

    transfer.setId((long) 1);
    transfer.setMontantTransfer(BigDecimal.TEN);
    transfer.setCompteBeneficiaire(compte2);
    transfer.setCompteEmetteur(compte1);
    transfer.setDateExecution(new Date());
    transfer.setMotifTransfer("Assignment 2021");
    Transfer transfer1 = transferRepository.save(transfer);

    //when
    Transfer FindOne = transferRepository.findById(transfer1.getId()+1).orElse(null);

    //then
    assertNull(FindOne);

  }

  @Test
  public void checkAllSavedUtilisateur()  {
    ArrayList<Transfer> listeDesTransferts =  (ArrayList<Transfer>) transferRepository.findAll();
    assertEquals(listeDesTransferts.size(),1);
  }

  @Test
  public void CheckSavingInDataBase() throws ParseException {
    //given
    Transfer transfer = new Transfer();

    Utilisateur utilisateur1 = new Utilisateur();
    utilisateur1.setUsername("user1");
    utilisateur1.setLastname("Ettafssaoui");
    utilisateur1.setFirstname("Youssef");
    utilisateur1.setGender(Gender.MALE);
    Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
    utilisateur1.setBirthdate(dateNaissanceUtilisateur1);

    Utilisateur utilisateur2 = new Utilisateur();
    utilisateur2.setUsername("user2");
    utilisateur2.setLastname("Ettafssaoui");
    utilisateur2.setFirstname("Ahmed");
    utilisateur2.setGender(Gender.MALE);
    Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
    utilisateur2.setBirthdate(dateNaissanceUtilisateur2);

    Compte compte1 = new Compte();
    compte1.setNumeroCompte("010000A000001000");
    compte1.setRib("RIB1");
    compte1.setSolde(BigDecimal.valueOf(200000L));
    compte1.setUtilisateur(utilisateur1);
    compteRepository.save(compte1);

    Compte compte2 = new Compte();
    compte2.setNumeroCompte("010000B025001000");
    compte2.setRib("RIB2");
    compte2.setSolde(BigDecimal.valueOf(140000L));
    compte2.setUtilisateur(utilisateur2);
    compteRepository.save(compte2);

    transfer.setCompteEmetteur(compte1);
    transfer.setDateExecution(new Date());
    transfer.setMontantTransfer(BigDecimal.TEN);
    transfer.setCompteBeneficiaire(compte2);
    transfer.setMotifTransfer("Assignment 2021");

    //when
    Transfer savedTransfer =  transferRepository.save(transfer);


    //then
    assertSame(transfer, savedTransfer);
  }

  @Test
  public void checkDeletingExistingUser() throws ParseException {

    //given
    Transfer transfer = new Transfer();

    Utilisateur utilisateur1 = new Utilisateur();
    utilisateur1.setUsername("user1");
    utilisateur1.setLastname("Ettafssaoui");
    utilisateur1.setFirstname("Youssef");
    utilisateur1.setGender(Gender.MALE);
    Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
    utilisateur1.setBirthdate(dateNaissanceUtilisateur1);

    Compte compte1 = new Compte();
    compte1.setNumeroCompte("010000A000001000");
    compte1.setRib("RIB1");
    compte1.setSolde(BigDecimal.valueOf(200000L));
    compte1.setUtilisateur(utilisateur1);
    compteRepository.save(compte1);

    Utilisateur utilisateur2 = new Utilisateur();
    utilisateur2.setUsername("user2");
    utilisateur2.setLastname("Ettafssaoui");
    utilisateur2.setFirstname("Ahmed");
    utilisateur2.setGender(Gender.MALE);
    Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
    utilisateur2.setBirthdate(dateNaissanceUtilisateur2);

    Compte compte2 = new Compte();
    compte2.setNumeroCompte("010000B025001000");
    compte2.setRib("RIB2");
    compte2.setSolde(BigDecimal.valueOf(140000L));
    compte2.setUtilisateur(utilisateur2);
    compteRepository.save(compte2);

    transfer.setCompteEmetteur(compte1);
    transfer.setCompteBeneficiaire(compte2);
    transfer.setMontantTransfer(BigDecimal.valueOf(150.5));
    transfer.setMotifTransfer("Poche");
    Transfer saved = transferRepository.save(transfer);
    transferRepository.delete(saved);

    //when
    Transfer found = transferRepository.findById(saved.getId()).orElse(null);

    //then
    assertNull(found);
  }
}
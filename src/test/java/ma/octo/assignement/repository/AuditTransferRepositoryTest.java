package ma.octo.assignement.repository;

import ma.octo.assignement.domain.*;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.domain.util.Gender;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;



@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class AuditTransferRepositoryTest {

    @Autowired
    AuditTransferRepository auditTransferRepository;



    @AfterEach
    void treatDown(){
        auditTransferRepository.deleteAll();
    }

    @Test
    public void checkExitingAuditById()  {

        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage("test");
        auditTransfer.setMessage("test message");
        AuditTransfer saved = auditTransferRepository.save(auditTransfer);
        AuditTransfer FindOne = auditTransferRepository.findById(saved.getId()).orElse(null);

        assert FindOne != null;
        assertEquals(FindOne.getId(), saved.getId());


    }

    @Test
    public void checkNonExitingAuditById()  {

        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage("test");
        auditTransfer.setEventType(EventType.DEPOSIT);
        AuditTransfer saved = auditTransferRepository.save(auditTransfer);
        AuditTransfer FindOne = auditTransferRepository.findById(saved.getId()+1).orElse(null);



        assertNull(FindOne);

    }

    @Test
    public void checkAllSavedAudits()  {
        ArrayList<AuditTransfer> all =  (ArrayList<AuditTransfer>) auditTransferRepository.findAll();
        assertEquals(all.size(),0);
    }

    @Test
    public void checkSavingInDataBase(){

        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage("test");
        auditTransfer.setMessage("test message");
        AuditTransfer saved = auditTransferRepository.save(auditTransfer);
        assertSame(auditTransfer, saved);
    }

    @Test
    public void checkDeletingDataBase(){

        AuditTransfer auditTransfer = new AuditTransfer();
        auditTransfer.setMessage("test");
        auditTransfer.setMessage("test message");
        AuditTransfer saved = auditTransferRepository.save(auditTransfer);
        auditTransferRepository.delete(saved);
        AuditTransfer found = auditTransferRepository.findById(saved.getId()).orElse(null);

        assertNull(found);
    }

}
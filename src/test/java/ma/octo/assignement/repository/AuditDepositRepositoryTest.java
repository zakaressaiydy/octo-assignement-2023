package ma.octo.assignement.repository;

import ma.octo.assignement.domain.AuditDeposit;
import ma.octo.assignement.domain.util.EventType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;



@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class AuditDepositRepositoryTest {


    @Autowired
    AuditDepositRepository auditDepositRepository;


    @AfterEach
    void treatDown(){
        auditDepositRepository.deleteAll();
    }

    @Test
    public void checkExitingAuditById()  {

        AuditDeposit auditDeposit = new AuditDeposit();
        auditDeposit.setMessage("test");
        auditDeposit.setEventType(EventType.DEPOSIT);
        AuditDeposit saved = auditDepositRepository.save(auditDeposit);
        AuditDeposit FindOne = auditDepositRepository.findById(saved.getId()).orElse(null);

        assert FindOne != null;
        assertEquals(FindOne.getId(), saved.getId());

    }


    @Test
    public void checkNonExitingAuditById()  {

        AuditDeposit auditDeposit = new AuditDeposit();
        auditDeposit.setMessage("test");
        auditDeposit.setEventType(EventType.DEPOSIT);
        AuditDeposit saved = auditDepositRepository.save(auditDeposit);
        AuditDeposit FindOne = auditDepositRepository.findById(saved.getId()+1).orElse(null);



        assertNull(FindOne);

    }

    @Test
    public void checkAllSavedAudits() throws ParseException {
        ArrayList<AuditDeposit> all =  (ArrayList<AuditDeposit>) auditDepositRepository.findAll();
        assertEquals(all.size(),0);
    }

    @Test
    public void checkSavingInDataBase() throws ParseException {

        AuditDeposit auditDeposit = new AuditDeposit();
        auditDeposit.setMessage("test");
        auditDeposit.setEventType(EventType.DEPOSIT);
        AuditDeposit saved = auditDepositRepository.save(auditDeposit);
        assertSame(auditDeposit, saved);
    }

    @Test
    public void checkDeletingDataBase() throws ParseException {

        AuditDeposit auditDeposit = new AuditDeposit();
        auditDeposit.setMessage("test");
        auditDeposit.setMessage("test message");

        AuditDeposit saved = auditDepositRepository.save(auditDeposit);
        auditDepositRepository.delete(saved);
        AuditDeposit found = auditDepositRepository.findById(saved.getId()).orElse(null);

        assertNull(found);
    }


}
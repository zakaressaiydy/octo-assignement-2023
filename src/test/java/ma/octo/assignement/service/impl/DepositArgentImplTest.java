package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.DepositArgent;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.DepositArgentDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.MontantNonAutoriserException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositArgentRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.ICompteService;
import ma.octo.assignement.service.IDepositArgentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class DepositArgentImplTest {


    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Autowired
    CompteRepository compteRepository;

    @Autowired
    ICompteService compteService;

    @Autowired
    IDepositArgentService depositArgentService;



    @Test
    void checkIfTransactionWillPassIfInfoIsCorrect() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException, ParseException, MontantNonAutoriserException {


        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);



        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        CompteMapper compteMapper = new CompteMapper();
        DepositArgentDto depositArgentDto = new DepositArgentDto();
        depositArgentDto.setMotifDeposit("octo");
        depositArgentDto.setMontant(new BigDecimal(100));
        depositArgentDto.setNomPrenomEmetteur("zakariae essaiydy");
        depositArgentDto.setDateExecution(new Date());
        depositArgentDto.setCompteBeneficiaire(compteMapper.toDto(compte1));



        //when
        DepositArgentDto saved = depositArgentService.depositArgent(depositArgentDto);

        //then

        assertEquals(saved.getMontant(),depositArgentDto.getMontant());
        assertEquals(saved.getMotifDeposit(),depositArgentDto.getMotifDeposit());
        assertEquals(saved.getDateExecution(),depositArgentDto.getDateExecution());









    }


    @Test
    void checkIfTransactionWillNotPassIfSenderNotValid() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException, ParseException {




        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);



        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);


        CompteMapper compteMapper = new CompteMapper();
        DepositArgentDto depositArgentDto = new DepositArgentDto();
        depositArgentDto.setMotifDeposit("octo");
        depositArgentDto.setMontant(new BigDecimal(100));
        depositArgentDto.setNomPrenomEmetteur("zakariae essaiydy");
        depositArgentDto.setDateExecution(new Date());
        depositArgentDto.setCompteBeneficiaire(compteMapper.toDto(compte1));

        //when

        CompteNonExistantException thrown = assertThrows(CompteNonExistantException.class, () ->
                depositArgentService.depositArgent(depositArgentDto));

        //then
        assertEquals("Compte Non existant", thrown.getMessage());

    }


    @Test
    void checkIfTransactionWillNotPassIfRecieverNotValid() throws ParseException {



        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);



        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        CompteMapper compteMapper = new CompteMapper();
        DepositArgentDto depositArgentDto = new DepositArgentDto();
        depositArgentDto.setMotifDeposit("octo");
        depositArgentDto.setMontant(new BigDecimal(100));
        depositArgentDto.setNomPrenomEmetteur("");
        depositArgentDto.setDateExecution(new Date());
        depositArgentDto.setCompteBeneficiaire(compteMapper.toDto(compte1));

        //when

        TransactionException thrown = assertThrows(TransactionException.class, () ->
                depositArgentService.depositArgent(depositArgentDto));

        //then
        assertEquals("nom vide", thrown.getMessage());

    }


    @Test
    void checkIfTransactionWillNotPassIfMontantEmpty() throws ParseException {


        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);



        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        CompteMapper compteMapper = new CompteMapper();
        DepositArgentDto depositArgentDto = new DepositArgentDto();
        depositArgentDto.setMotifDeposit("octo");
        depositArgentDto.setMontant(new BigDecimal(0));
        depositArgentDto.setNomPrenomEmetteur("zakariae essaiydy");
        depositArgentDto.setDateExecution(new Date());
        depositArgentDto.setCompteBeneficiaire(compteMapper.toDto(compte1));

        //when

        TransactionException thrown = assertThrows(TransactionException.class, () ->
                depositArgentService.depositArgent(depositArgentDto));

        //then
        assertEquals("Montant invalid", thrown.getMessage());

    }


    @Test
    void checkIfTransactionWillNotPassIfMontantGreaterThen10000() throws ParseException {



        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);



        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        CompteMapper compteMapper = new CompteMapper();
        DepositArgentDto depositArgentDto = new DepositArgentDto();
        depositArgentDto.setMotifDeposit("octo");
        depositArgentDto.setMontant(new BigDecimal(100200));
        depositArgentDto.setNomPrenomEmetteur("zakariae essaiydy");
        depositArgentDto.setDateExecution(new Date());
        depositArgentDto.setCompteBeneficiaire(compteMapper.toDto(compte1));

        //when

        MontantNonAutoriserException thrown = assertThrows(MontantNonAutoriserException.class, () ->
                depositArgentService.depositArgent(depositArgentDto));

        //then
        assertEquals("Le montant maximal est 10000", thrown.getMessage());

    }



    @Test
    void checkIfTransactionWilNotPassIfMotifNotValid() throws ParseException {




        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);



        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        CompteMapper compteMapper = new CompteMapper();
        DepositArgentDto depositArgentDto = new DepositArgentDto();
        depositArgentDto.setMotifDeposit("");
        depositArgentDto.setMontant(new BigDecimal(100));
        depositArgentDto.setNomPrenomEmetteur("zakariae essaiydy");
        depositArgentDto.setDateExecution(new Date());
        depositArgentDto.setCompteBeneficiaire(compteMapper.toDto(compte1));

        //when
        TransactionException thrown = assertThrows(TransactionException.class, () ->
                depositArgentService.depositArgent(depositArgentDto));

        //then
        assertEquals("Votre MotifDeposit est vide", thrown.getMessage());


    }




}
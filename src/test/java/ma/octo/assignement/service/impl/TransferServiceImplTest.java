package ma.octo.assignement.service.impl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.TransferRepositoryTest;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.service.ITransferService;
import ma.octo.assignement.service.common.Utils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class TransferServiceImplTest {

    @Autowired
    CompteRepository compteRepository;

    @Autowired
    UtilisateurRepository utilisateurRepository;


    @Autowired
    TransferRepository transferRepository;

    @Autowired
    ITransferService transferService;

    @Test
    void checkIfTransactionWillPassIfInfoIsCorrect() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException, ParseException {


        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("Ettafssaoui");
        utilisateur2.setFirstname("Ahmed");
        utilisateur2.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
        utilisateur2.setBirthdate(dateNaissanceUtilisateur2);
        utilisateurRepository.save(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNumeroCompte("B025001000");
        compte2.setRib("RIB20");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);
        compteRepository.save(compte2);

        CompteMapper compteMapper = new CompteMapper();

        TransferDto transferDto = new TransferDto();
        transferDto.setMontantTransfer(new BigDecimal(200));
        transferDto.setMotifTransfer("octo");
        transferDto.setDateExecution(new Date());
        transferDto.setCompteBeneficiaire(compteMapper.toDto(compte1));
        transferDto.setCompteEmetteur(compteMapper.toDto(compte2));


        //when
        TransferDto saved = transferService.createTransaction(transferDto);

        //then

        assertEquals(saved.getMontantTransfer(),transferDto.getMontantTransfer());
        assertEquals(saved.getMotifTransfer(),transferDto.getMotifTransfer());
        assertEquals(saved.getDateExecution(),transferDto.getDateExecution());









    }


    @Test
    void checkIfTransactionWillNotPassIfSenderNotValid() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException, ParseException {



        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("Ettafssaoui");
        utilisateur2.setFirstname("Ahmed");
        utilisateur2.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
        utilisateur2.setBirthdate(dateNaissanceUtilisateur2);
        utilisateurRepository.save(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNumeroCompte("B025001000");
        compte2.setRib("RIB20");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);


        CompteMapper compteMapper = new CompteMapper();

        TransferDto transferDto = new TransferDto();
        transferDto.setMontantTransfer(new BigDecimal(200));
        transferDto.setMotifTransfer("octo");
        transferDto.setDateExecution(new Date());
        transferDto.setCompteBeneficiaire(compteMapper.toDto(compte1));
        transferDto.setCompteEmetteur(compteMapper.toDto(compte2));


        //when

        CompteNonExistantException thrown = assertThrows(CompteNonExistantException.class, () ->
                transferService.createTransaction(transferDto));

        //then
        assertEquals("Compte Non existant", thrown.getMessage());

    }


    @Test
    void checkIfTransactionWillNotPassIfRecieverNotValid() throws ParseException {


        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("Ettafssaoui");
        utilisateur2.setFirstname("Ahmed");
        utilisateur2.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
        utilisateur2.setBirthdate(dateNaissanceUtilisateur2);
        utilisateurRepository.save(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);


        Compte compte2 = new Compte();
        compte2.setNumeroCompte("B025001000");
        compte2.setRib("RIB20");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);
        compteRepository.save(compte2);


        CompteMapper compteMapper = new CompteMapper();

        TransferDto transferDto = new TransferDto();
        transferDto.setMontantTransfer(new BigDecimal(200));
        transferDto.setMotifTransfer("octo");
        transferDto.setDateExecution(new Date());
        transferDto.setCompteBeneficiaire(compteMapper.toDto(compte1));
        transferDto.setCompteEmetteur(compteMapper.toDto(compte2));


        //when

        CompteNonExistantException thrown = assertThrows(CompteNonExistantException.class, () ->
                transferService.createTransaction(transferDto));

        //then
        assertEquals("Compte Non existant", thrown.getMessage());

    }


    @Test
    void checkIfTransactionWillNotPassIfMontantLessThen10() throws ParseException {

        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("Ettafssaoui");
        utilisateur2.setFirstname("Ahmed");
        utilisateur2.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
        utilisateur2.setBirthdate(dateNaissanceUtilisateur2);
        utilisateurRepository.save(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNumeroCompte("B025001000");
        compte2.setRib("RIB20");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);
        compteRepository.save(compte2);


        CompteMapper compteMapper = new CompteMapper();

        TransferDto transferDto = new TransferDto();
        transferDto.setMontantTransfer(new BigDecimal(5));
        transferDto.setMotifTransfer("octo");
        transferDto.setDateExecution(new Date());
        transferDto.setCompteBeneficiaire(compteMapper.toDto(compte1));
        transferDto.setCompteEmetteur(compteMapper.toDto(compte2));


        //when

        TransactionException thrown = assertThrows(TransactionException.class, () ->
                transferService.createTransaction(transferDto));

        //then
        assertEquals("Montant minimal de transfer non atteint", thrown.getMessage());

    }


    @Test
    void checkIfTransactionWillNotPassIfMontantGreaterThen10000() throws ParseException {


        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("Ettafssaoui");
        utilisateur2.setFirstname("Ahmed");
        utilisateur2.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
        utilisateur2.setBirthdate(dateNaissanceUtilisateur2);
        utilisateurRepository.save(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNumeroCompte("B025001000");
        compte2.setRib("RIB20");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);
        compteRepository.save(compte2);


        CompteMapper compteMapper = new CompteMapper();

        TransferDto transferDto = new TransferDto();
        transferDto.setMontantTransfer(new BigDecimal(1000000000));
        transferDto.setMotifTransfer("octo");
        transferDto.setDateExecution(new Date());
        transferDto.setCompteBeneficiaire(compteMapper.toDto(compte1));
        transferDto.setCompteEmetteur(compteMapper.toDto(compte2));


        //when

        TransactionException thrown = assertThrows(TransactionException.class, () ->
                transferService.createTransaction(transferDto));

        //then
        assertEquals("Montant maximal de transfer dépassé", thrown.getMessage());

    }



    @Test
    void checkIfTransactionWilNotPassIfMotifNotValid() throws ParseException {



        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("Ettafssaoui");
        utilisateur2.setFirstname("Ahmed");
        utilisateur2.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
        utilisateur2.setBirthdate(dateNaissanceUtilisateur2);
        utilisateurRepository.save(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNumeroCompte("B025001000");
        compte2.setRib("RIB20");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);
        compteRepository.save(compte2);


        CompteMapper compteMapper = new CompteMapper();

        TransferDto transferDto = new TransferDto();
        transferDto.setMontantTransfer(new BigDecimal(100));
        transferDto.setMotifTransfer("");
        transferDto.setDateExecution(new Date());
        transferDto.setCompteBeneficiaire(compteMapper.toDto(compte1));
        transferDto.setCompteEmetteur(compteMapper.toDto(compte2));


        //when

        TransactionException thrown = assertThrows(TransactionException.class, () ->
                transferService.createTransaction(transferDto));

        //then
        assertEquals("Motif vide", thrown.getMessage());

    }


    @Test
    void checkIfTransactionWillNotPassWithNoEnoughtMoney() throws ParseException {

        //given

        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("Ettafssaoui");
        utilisateur1.setFirstname("Youssef");
        utilisateur1.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur1 = new SimpleDateFormat("dd-MM-yyyy").parse("08-02-1999");
        utilisateur1.setBirthdate(dateNaissanceUtilisateur1);
        utilisateurRepository.save(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("Ettafssaoui");
        utilisateur2.setFirstname("Ahmed");
        utilisateur2.setGender(Gender.MALE);
        Date dateNaissanceUtilisateur2 = new SimpleDateFormat("dd-MM-yyyy").parse("29-01-1945");
        utilisateur2.setBirthdate(dateNaissanceUtilisateur2);
        utilisateurRepository.save(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNumeroCompte("A00000001000");
        compte1.setRib("RIB10");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);
        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNumeroCompte("B025001000");
        compte2.setRib("RIB20");
        compte2.setSolde(BigDecimal.valueOf(140L));
        compte2.setUtilisateur(utilisateur2);
        compteRepository.save(compte2);


        CompteMapper compteMapper = new CompteMapper();

        TransferDto transferDto = new TransferDto();
        transferDto.setMontantTransfer(new BigDecimal(1000));
        transferDto.setMotifTransfer("octo");
        transferDto.setDateExecution(new Date());
        transferDto.setCompteBeneficiaire(compteMapper.toDto(compte1));
        transferDto.setCompteEmetteur(compteMapper.toDto(compte2));


        //when

        SoldeDisponibleInsuffisantException thrown = assertThrows(SoldeDisponibleInsuffisantException.class, () ->
                transferService.createTransaction(transferDto));

        //then
        assertEquals("Solde insuffisant pour l'utilisateur", thrown.getMessage());

    }


}
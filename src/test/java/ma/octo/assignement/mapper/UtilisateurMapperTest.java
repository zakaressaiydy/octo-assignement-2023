package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.UtilisateurDto;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;



class UtilisateurMapperTest {




    @Test
    void shouldProperlyMapUtilisateurToDto() {

        UtilisateurMapper utilisateurMapper = new UtilisateurMapper();

        //given
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setFirstname("zakariae");
        utilisateur.setLastname("Essaiydy");
        utilisateur.setUsername("ziko");
        utilisateur.setGender(Gender.MALE);
        utilisateur.setBirthdate(new Date());
        utilisateur.setId(1L);


        //when
        UtilisateurDto utilisateurDto = utilisateurMapper.toDto(utilisateur);


        //then

        assertEquals(utilisateurDto.getId(),utilisateur.getId());
        assertEquals(utilisateurDto.getBirthdate(),utilisateur.getBirthdate());
        assertEquals(utilisateurDto.getFirstname(),utilisateur.getFirstname());
        assertEquals(utilisateurDto.getLastname(),utilisateur.getLastname());
        assertEquals(utilisateurDto.getGender(),utilisateur.getGender());



    }

    @Test
    void shouldProperlyMapUtilisateurDtoToModel() {



        UtilisateurMapper utilisateurMapper = new UtilisateurMapper();

        //given
        UtilisateurDto utilisateurDto = new UtilisateurDto();
        utilisateurDto.setFirstname("zakariae");
        utilisateurDto.setLastname("Essaiydy");
        utilisateurDto.setUsername("ziko");
        utilisateurDto.setGender(Gender.MALE);
        utilisateurDto.setBirthdate(new Date());
        utilisateurDto.setId(1L);


        //when
        Utilisateur utilisateur = utilisateurMapper.toModel(utilisateurDto);


        //then

        assertEquals(utilisateurDto.getId(),utilisateur.getId());
        assertEquals(utilisateurDto.getBirthdate(),utilisateur.getBirthdate());
        assertEquals(utilisateurDto.getFirstname(),utilisateur.getFirstname());
        assertEquals(utilisateurDto.getLastname(),utilisateur.getLastname());
        assertEquals(utilisateurDto.getGender(),utilisateur.getGender());



    }
}
package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.DepositArgent;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.DepositArgentDto;
import ma.octo.assignement.dto.UtilisateurDto;
import org.junit.jupiter.api.Test;

import javax.xml.crypto.Data;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class DepositArgentMapperTest {

    @Test
    void shouldProperlyMapDepositArgentToDto() {

        DepositArgentMapper depositArgentMapper = new DepositArgentMapper();



        //given
        DepositArgent depositArgent = new DepositArgent();
        depositArgent.setMotifDeposit("octo");
        depositArgent.setDateExecution(new Date());
        depositArgent.setId(1L);
        depositArgent.setMontant(new BigDecimal(100));
        Compte compte = new Compte();
        compte.setRib("RIB5");
        compte.setNumeroCompte("1234567890");
        compte.setSolde(new BigDecimal("19980.9"));
        compte.setId(1L);
        compte.setUtilisateur(new Utilisateur(1L,"zako", Gender.MALE,"essaiydy","zakariae",new Date()));
        depositArgent.setCompteBeneficiaire(compte);

        //when

        DepositArgentDto depositArgentDto = depositArgentMapper.toDto(depositArgent);


        //then


        assertEquals(depositArgent.getMotifDeposit(),depositArgentDto.getMotifDeposit());
        assertEquals(depositArgent.getNomPrenomEmetteur(),depositArgentDto.getNomPrenomEmetteur());
        assertEquals(depositArgent.getDateExecution(),depositArgentDto.getDateExecution());
        assertEquals(depositArgent.getId(),depositArgentDto.getId());
        assertEquals(depositArgent.getMontant(),depositArgentDto.getMontant());
        assertEquals(depositArgent.getCompteBeneficiaire().getNumeroCompte(),depositArgentDto.getCompteBeneficiaire().getNumeroCompte());




    }

    @Test
    void shouldProperlyMapDepositArgentDtoToModel() {


        DepositArgentMapper depositArgentMapper = new DepositArgentMapper();



        //given
        DepositArgentDto depositArgentDto = new DepositArgentDto();
        depositArgentDto.setMotifDeposit("octo");
        depositArgentDto.setDateExecution(new Date());
        depositArgentDto.setId(1L);
        depositArgentDto.setMontant(new BigDecimal(100));
        CompteDto compteDto = new CompteDto();
        compteDto.setRib("RIB5");
        compteDto.setNumeroCompte("1234567890");
        compteDto.setSolde(new BigDecimal("19980.9"));
        compteDto.setId(1L);
        compteDto.setUtilisateur(new UtilisateurDto(1L,"zako", Gender.MALE,"essaiydy","zakariae",new Date()));
        depositArgentDto.setCompteBeneficiaire(compteDto);

        //when

        DepositArgent depositArgent = depositArgentMapper.toModel(depositArgentDto);


        //then


        assertEquals(depositArgent.getMotifDeposit(),depositArgentDto.getMotifDeposit());
        assertEquals(depositArgent.getNomPrenomEmetteur(),depositArgentDto.getNomPrenomEmetteur());
        assertEquals(depositArgent.getDateExecution(),depositArgentDto.getDateExecution());
        assertEquals(depositArgent.getId(),depositArgentDto.getId());
        assertEquals(depositArgent.getMontant(),depositArgentDto.getMontant());
        assertEquals(depositArgent.getCompteBeneficiaire().getNumeroCompte(),depositArgentDto.getCompteBeneficiaire().getNumeroCompte());




    }
}
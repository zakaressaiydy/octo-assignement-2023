package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.dto.UtilisateurDto;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TransferMapperTest {

    @Test
    void shouldProperlyMapTransferToDto() {
        TransferMapper transferMapper = new TransferMapper();

        //given
        Transfer transfer = new Transfer();
        transfer.setMotifTransfer("octo");
        transfer.setMontantTransfer(new BigDecimal(100));
        transfer.setDateExecution(new Date());
        transfer.setId(1L);
        transfer.setCompteBeneficiaire(
                new Compte(
                        1L,
                        "1234567890",
                        "Rib1",
                        new BigDecimal(19999),
                        new Utilisateur(1L,"zako", Gender.MALE,"essaiydy","zakariae",new Date())
                        ));

        transfer.setCompteEmetteur(
                new Compte(
                        1L,
                        "100000067890",
                        "Rib2",
                        new BigDecimal(18888899),
                        new Utilisateur(1L,"Mako", Gender.MALE,"eaiydy","zariae",new Date())
                )


        );

        //when
        TransferDto transferDto =transferMapper.toDto(transfer);



        //then


        assertEquals(transfer.getId(),transferDto.getId());
        assertEquals(transfer.getMotifTransfer(),transferDto.getMotifTransfer());
        assertEquals(transfer.getMontantTransfer(),transferDto.getMontantTransfer());
        assertEquals(transfer.getDateExecution(),transferDto.getDateExecution());
        assertEquals(transfer.getCompteEmetteur().getNumeroCompte(),transferDto.getCompteEmetteur().getNumeroCompte());
        assertEquals(transfer.getCompteBeneficiaire().getNumeroCompte(),transferDto.getCompteBeneficiaire().getNumeroCompte());



    }

    @Test
    void shouldProperlyMapTransferDtoToModel() {


        TransferMapper transferMapper = new TransferMapper();

        //given
        TransferDto transferDto = new TransferDto();
        transferDto.setMotifTransfer("octo");
        transferDto.setMontantTransfer(new BigDecimal(100));
        transferDto.setDateExecution(new Date());
        transferDto.setId(1L);
        transferDto.setCompteBeneficiaire(
                new CompteDto(
                        1L,
                        "1234567890",
                        "Rib1",
                        new BigDecimal(19999),
                        new UtilisateurDto(1L,"zako", Gender.MALE,"essaiydy","zakariae",new Date())
                ));

        transferDto.setCompteEmetteur(
                new CompteDto(
                        1L,
                        "100000067890",
                        "Rib2",
                        new BigDecimal(18888899),
                        new UtilisateurDto(1L,"Mako", Gender.MALE,"eaiydy","zariae",new Date())
                )


        );

        //when
        Transfer transfer =transferMapper.toModel(transferDto);



        //then


        assertEquals(transfer.getId(),transferDto.getId());
        assertEquals(transfer.getMotifTransfer(),transferDto.getMotifTransfer());
        assertEquals(transfer.getMontantTransfer(),transferDto.getMontantTransfer());
        assertEquals(transfer.getDateExecution(),transferDto.getDateExecution());
        assertEquals(transfer.getCompteEmetteur().getNumeroCompte(),transferDto.getCompteEmetteur().getNumeroCompte());
        assertEquals(transfer.getCompteBeneficiaire().getNumeroCompte(),transferDto.getCompteBeneficiaire().getNumeroCompte());


    }
}
package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class CompteMapperTest {

    @Test
    void shouldProperlyMapCompteToDto() {

        CompteMapper compteMapper = new CompteMapper();
        UtilisateurMapper utilisateurMapper = new UtilisateurMapper();


        //given
        Compte compte = new Compte();
        compte.setRib("RIB5");
        compte.setNumeroCompte("1234567890");
        compte.setSolde(new BigDecimal("19980.9"));
        compte.setId(1L);
        compte.setUtilisateur(new Utilisateur(1L,"zako", Gender.MALE,"essaiydy","zakariae",new Date()));


        //when

        CompteDto compteDto = compteMapper.toDto(compte);

        //then

        assertEquals(compte.getId(),compteDto.getId());
        assertEquals(compte.getNumeroCompte(),compteDto.getNumeroCompte());
        assertEquals(compte.getRib(),compteDto.getRib());
        assertEquals(compte.getSolde(),compteDto.getSolde());
        assertEquals(compte.getUtilisateur().getId(),compteDto.getUtilisateur().getId());



    }

    @Test
    void shouldProperlyMapCompteDtoToModel() {

        CompteMapper compteMapper = new CompteMapper();
        UtilisateurMapper utilisateurMapper = new UtilisateurMapper();


        //given
        CompteDto compteDto = new CompteDto();
        compteDto.setRib("RIB5");
        compteDto.setNumeroCompte("1234567890");
        compteDto.setSolde(new BigDecimal("19980.9"));
        compteDto.setId(1L);
        compteDto.setUtilisateur(new UtilisateurDto(1L,"zako", Gender.MALE,"essaiydy","zakariae",new Date()));


        //when

        Compte compte = compteMapper.toModel(compteDto);

        //then

        assertEquals(compte.getId(),compteDto.getId());
        assertEquals(compte.getNumeroCompte(),compteDto.getNumeroCompte());
        assertEquals(compte.getRib(),compteDto.getRib());
        assertEquals(compte.getSolde(),compteDto.getSolde());
        assertEquals(compte.getUtilisateur().getId(),compteDto.getUtilisateur().getId());




    }
}